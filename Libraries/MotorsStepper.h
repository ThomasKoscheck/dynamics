#ifndef Drive_h
#define Drive_h
#include "Wire.h"
#include "Adafruit_MotorShield.h"
Adafruit_MotorShield Shield = Adafruit_MotorShield(0x60);
Adafruit_StepperMotor *linkerMotor = Shield.getStepper(200, 1);
Adafruit_StepperMotor *rechterMotor = Shield.getStepper(200,2);
byte stepmenge = 1;

void StepperInit()  {
    Shield.begin(1600);
    linkerMotor->setSpeed(250);
    rechterMotor->setSpeed(250);
}


void Vorwaertsfahren(int steps = 5) {
    while(steps > 0)
    {
        steps--;
        linkerMotor->step(stepmenge, FORWARD, DOUBLE);
        rechterMotor->step(stepmenge, FORWARD, DOUBLE);
    }
}
void Zurueckfahren(int steps = 5) {
    while(steps > 0)
    {
        steps--;
        linkerMotor->step(stepmenge, BACKWARD, DOUBLE);
        rechterMotor->step(stepmenge, BACKWARD, DOUBLE);
    }
}
void Rechtsfahren(int steps = 5) {
    while(steps > 0)
    {
        steps--;
        linkerMotor->step(stepmenge, FORWARD, DOUBLE);
        rechterMotor->step(stepmenge, BACKWARD, DOUBLE);
    }
}
void Linksfahren(int steps = 5) {
    while(steps > 0)
    {
        steps--;
        linkerMotor->step(stepmenge, BACKWARD, DOUBLE);
        rechterMotor->step(stepmenge, FORWARD, DOUBLE);
    }
}



#endif
