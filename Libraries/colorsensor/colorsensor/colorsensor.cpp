#include "colorsensor.h"
#include "Arduino.h"


//Library for reading and interpreting the values of a TCS2730 sensor for line tracking by Max Appel
//License: CC BY-SA

colorsensor::colorsensor(short *leftType, short *midType, short *rightType, short *sensorType, bool greenSelector)
{
    this->leftPin = 8;
    this->midPin = 9;
    this->rightPin = 10;
    this->leftType = leftType;
    this->midType = midType;
    this->rightType = rightType;
    this->greenBorder = 10;
    this->s2 = 6;
    this->s3 = 7;
    this->green=greenSelector;
    pinMode(s2, OUTPUT);
    pinMode(s3, OUTPUT);
    pinMode(leftPin, INPUT);
    pinMode(midPin, INPUT);
    pinMode(rightPin, INPUT);

}

void colorsensor::changeSensorPin(short leftPin, short midPin, short rightPin)  {
    this->leftPin = leftPin;
    this->midPin = midPin;
    this->rightPin = rightPin;
}

void colorsensor::read()    {
    if(green)   {
            setRed();
            delayMicroseconds(100);
            leftRedValue = readPin(&leftPin);
            midRedValue = readPin(&midPin);
            rightRedValue = readPin(&rightPin);
            setGreen();
            delayMicroseconds(100);
    }
    leftGreenValue = readPin(&leftPin);
    midGreenValue = readPin(&midPin);
    rightGreenValue = readPin(&rightPin);
    *leftType = green ? leftSelector(&leftGreenValue, &leftRedValue):selector(&leftGreenValue);
    *midType = green ? midSelector(&midGreenValue, &midRedValue) : selector(&midGreenValue);
    *rightType = green ? rightSelector(&rightGreenValue, &rightRedValue) : selector(&rightGreenValue);
    *sensorType = 0;
    *sensorType = (*leftType*100)+(*midType*10)+*rightType;
}

void colorsensor::setGreen()    {
    digitalWrite(s2, HIGH);
    digitalWrite(s3, HIGH);
}

void colorsensor::setRed()  {
    digitalWrite(s2, LOW);
    digitalWrite(s3, LOW);
}

void colorsensor::changeBorder(uint16_t greenBorder)   {
    this->greenBorder = greenBorder;
}

byte colorsensor::selector(uint16_t *greenValue)  {
    return greenBorder>*greenValue ? 1 : 2;
}

byte colorsensor::leftSelector(uint16_t *greenValue, uint16_t *redValue)    {
    byte returnValue = 0;
    if((*greenValue == 13 && *redValue == 17)||(*greenValue == 13 && *redValue == 16)||(*greenValue == 10 && *redValue == 10)||(*greenValue == 12 && *redValue == 15)||(*greenValue == 14 && *redValue == 17)||(*greenValue == 12 && *redValue == 15)||(*greenValue == 15 && *redValue == 19)||(*greenValue == 15 && *redValue == 20)||(*greenValue == 12 && *redValue == 15)||(*greenValue == 16 && *redValue == 21)||(*greenValue == 17 && *redValue == 21)||(*greenValue == 17 && *redValue == 25)||(*greenValue == 17 && *redValue == 24)||(*greenValue == 19 && *redValue == 26)||(*greenValue == 18 && *redValue == 21))  {
        returnValue = 3;
    }
    else if(*greenValue > 9)   {
        returnValue = 2;
    }
    else {
        returnValue = 1;
    }

    return returnValue;
}

byte colorsensor::midSelector(uint16_t *greenValue, uint16_t *redValue)    {
    byte returnValue = 0;
    if((*greenValue == 15 && *redValue == 18)||(*greenValue == 15 && *redValue == 17)||(*greenValue == 15 && *redValue == 19)||(*greenValue == 12 && *redValue == 14)||(*greenValue == 13 && *redValue == 14)||(*greenValue == 14 && *redValue == 15)||(*greenValue == 15 && *redValue == 16)||(*greenValue == 15 && *redValue == 17)||(*greenValue == 15 && *redValue == 18)||(*greenValue == 16 && *redValue == 18)||(*greenValue == 16 && *redValue == 19)||(*greenValue == 17 && *redValue == 19)||(*greenValue == 17 && *redValue == 20)||(*greenValue == 18 && *redValue == 20)||(*greenValue == 18 && *redValue == 21)||(*greenValue == 19 && *redValue == 21)||(*greenValue == 19 && *redValue == 22))  {
        returnValue = 3;
    }
    else if(*greenValue > 9)   {
        returnValue = 2;
    }
    else {
        returnValue = 1;
    }

    return returnValue;
}

byte colorsensor::getLeft(void) {
    read();
    return *leftType;
}

byte colorsensor::getMid(void)  {
    read();
    return *midType;
}

byte colorsensor::getRight(void)    {
    read();
    return *rightType;
}

byte colorsensor::rightSelector(uint16_t *greenValue, uint16_t *redValue)    {
    byte returnValue = 0;
    if((*greenValue == 20 && *redValue == 27)||(*greenValue == 20 && *redValue == 26)||(*greenValue == 19 && *redValue == 27)||(*greenValue == 13 && *redValue == 14)||(*greenValue == 14 && *redValue == 16)||(*greenValue == 14 && *redValue == 17)||(*greenValue == 15 && *redValue == 18)||(*greenValue == 16 && *redValue == 18)||(*greenValue == 16 && *redValue == 19)||(*greenValue == 17 && *redValue == 20)||(*greenValue == 18 && *redValue == 22)||(*greenValue == 18 && *redValue == 23)||(*greenValue == 19 && *redValue == 23)||(*greenValue == 19 && *redValue == 24)||(*greenValue == 19 && *redValue == 25)||(*greenValue == 20 && *redValue == 25)||(*greenValue == 20 && *redValue == 26)||(*greenValue == 21 && *redValue == 26)||(*greenValue == 22 && *redValue == 27))  {
        returnValue = 3;
    }
    else if(*greenValue > 9)   {
        returnValue = 2;
    }
    else {
        returnValue = 1;
    }

    return returnValue;
}

uint16_t colorsensor::readPin(short *pin)  {
    return pulseIn(*pin, digitalRead(*pin) == HIGH ? LOW : HIGH);
}
