
//Library for reading and interpreting the values of a TCS2730 sensor for line tracking by Max Appel
//License: CC BY-SA

#ifndef colorsensor_h
#define colorsensor_h

#include "Arduino.h"



class colorsensor
{
public:
    void read();
    void changeBorder(uint16_t greenBorder);
    uint16_t leftGreenValue, leftRedValue;
    uint16_t midGreenValue, midRedValue;
    uint16_t rightGreenValue, rightRedValue;
    void changeSensorPin(short leftPin, short midPin, short rightPin);
    colorsensor(short *leftType, short *midType, short *rightType, short *sensorType, bool greenSelector);
    byte getLeft(void);
    byte getMid(void);
    byte getRight(void);
private:
    bool green;
    short leftPin;
    short midPin;
    short rightPin;
    uint16_t readPin(short *pin);
    short *rightType;
    short *leftType;
    short *midType;
    short *sensorType;
    byte selector(uint16_t *greenValue);
    uint16_t greenBorder;
    void setRed();
    void setGreen();
    byte s2;
    byte s3;
    byte leftSelector(uint16_t *greenValue, uint16_t *redValue);
    byte midSelector(uint16_t *greenValue, uint16_t *redValue);
    byte rightSelector(uint16_t *greenValue, uint16_t *redValue);
};


#endif
