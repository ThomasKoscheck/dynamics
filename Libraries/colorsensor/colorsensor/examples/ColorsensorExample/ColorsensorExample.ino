/*Example Sketch for the colorsensor library*/

#include "colorsensor.h"

/*
leftpin = 10;   //Pin der Daten des Sensors
midpin = 9;
rightpin = 8;
*/

short linksTyp;     //WAs für eine Farbe? Wert 1 2 oder 3 wird hier hineingeschrieben
short mitteTyp;
short rechtsTyp;
 
colorsensor sensor = colorsensor(&linksTyp, &mitteTyp, &rechtsTyp, true); //true || false defines wether the redValue is being used to detect a green dot

void setup() {
  Serial.begin(115200);
}

void loop() {
  delay(100);
  sensor.read();    //lässt den Sensor "sensor" die Daten in den Variablen Typ (siehe oben) aktualisieren
  Serial.println(linksTyp);
  Serial.println(mitteTyp);
  Serial.println(rechtsTyp);
  Serial.println("\nGreen L|M|R");
  Serial.println(sensor.leftGreenValue);
  Serial.println(sensor.midGreenValue);
  Serial.println(sensor.rightGreenValue);
  Serial.println("\nRed L|M|R");
  Serial.println(sensor.leftRedValue);
  Serial.println(sensor.midRedValue);
  Serial.println(sensor.rightRedValue);
  delay(1900);
  Serial.println("----");
}

/* andere Methoden*/
void ChangeBorderBeispiel(void)  {
  uint16_t borderGreen = 20;    //16bit Wert - neues Grenzwert für Farbe Gruen
  sensor.changeBorder(borderGreen);
}

void ChangeSensorPinBeispiel(void)  { //nan
  short leftPin = 40;         //8bit value - Pin of the Data-Input of the sensor
  short midPin = 41;
  short rightPin = 42;
  sensor.changeSensorPin(leftPin, midPin, rightPin);
}

