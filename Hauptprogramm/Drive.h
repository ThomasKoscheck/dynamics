extern void LineUpdate();
extern void Vorwaertsfahren(int steps);
extern void Zurueckfahren(int steps);
extern void Linksfahren(int steps);
extern void Rechtsfahren(int steps);
extern void stop(void);
extern void setSpeedLeft(uint16_t speed);
extern void setSpeedRight(uint16_t speed);
extern const int einKaestchen;
extern void sout(String output);
extern bool farLeftBool;
extern bool leftBool;
extern bool midBool;
extern bool rightBool;
extern bool farRightBool;

int counter = 0;
const int borderCounter = 1000;
const short stepsKorrektur = 35;



void Reinigung()  {
    Zurueckfahren(300);
    setSpeedLeft(255);
    setSpeedRight(255);
    while(true) {
        Vorwaertsfahren();
    }
}
extern void sensorDiagnose();
extern void soutB(bool output);
void hindernisAusweichen()    {

/*  //Rammbock


  Zurueckfahren(200);
  setSpeedLeft(255);
  setSpeedRight(255);
  Vorwaertsfahren(700);
  LineUpdate();
  uint16_t counter = 0;
  while(!(leftBool && midBool && rightBool && !toggleDaihatsu))  {
        sensorDiagnose();
        soutB(midBool);
        soutB(rightBool);
        soutB(leftBool);
        sout("");

        Vorwaertsfahren(einKaestchen);
        counter++;
        LineUpdate();

        if(counter > 45)  {
            break;
        }

    }
    if(true) {
        Vorwaertsfahren(6*einKaestchen);
        stop();
        delay(1000);
        counter += 4;
        while(counter != 0 && midBool) {
            Zurueckfahren(einKaestchen);
            LineUpdate();
            counter--;
        }
    }
  setSpeedLeft();
  setSpeedRight();

 }*/ //Ende Rammbock


 //Umfahren
    Zurueckfahren(250);
    Rechtsfahren(220);
    Vorwaertsfahren(600);
    Linksfahren(200);
    Vorwaertsfahren(320);
    Linksfahren(200);
    LineUpdate();
    while(midBool) {
        LineUpdate();
        Vorwaertsfahren(1);
    }
    Vorwaertsfahren(100);
    LineUpdate();
    while(midBool) {
        LineUpdate();
        Rechtsfahren(1);
    }

}

void turnright(void)	{
    counter = 0;
    LineUpdate();
    while(!(leftBool && !midBool && rightBool) && counter < borderCounter)	{
        Rechtsfahren(1);
        LineUpdate();
        counter ++;
    }
    if (counter >= borderCounter){  //verhindert, dass er sich im Kreis dreht
        Linksfahren(borderCounter);
        Zurueckfahren(3*einKaestchen);
    }
    else{
      Rechtsfahren(stepsKorrektur);
    }
 }

void turnleft(void)	{
    counter = 0;
    LineUpdate();
    while(!(leftBool && !midBool && rightBool) && counter < borderCounter)  {
        Linksfahren(1);
        LineUpdate();
        counter ++;
    }
    if (counter >= borderCounter){  //verhindert, dass er sich im Kreis dreht
        Rechtsfahren(borderCounter);
        Zurueckfahren(3*einKaestchen);
    }
    else{
      Linksfahren(stepsKorrektur);
    }
}


