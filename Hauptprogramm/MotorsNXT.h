#ifndef motor2_h
#define motor2_h
#include "Adafruit_MotorShield.h"

volatile uint_fast32_t steps = 0;

Adafruit_MotorShield Shield = Adafruit_MotorShield(0x60);
Adafruit_DCMotor *linkerMotor = Shield.getMotor(4);
Adafruit_DCMotor *rechterMotor= Shield.getMotor(3);
void setSpeedLeft(uint16_t speed = 104)  {
    linkerMotor->setSpeed(speed);
}

void setSpeedRight(uint16_t speed = 104)    {
    rechterMotor->setSpeed(speed);
}

void stop(void) {
    rechterMotor->run(RELEASE);
    linkerMotor->run(RELEASE);
}

void encode(void)   {
    steps--;
}

bool testStep(void) {
    return steps > 10;
}

void setStepCount(uint_fast32_t stepCount)  {
    steps = stepCount;
}

void StepperInit()  {
    Shield.begin();
    linkerMotor->setSpeed(110);
    rechterMotor->setSpeed(110);
    attachInterrupt(0, encode, FALLING);
}

void Vorwaertsfahren(int steps = 1) {
    setStepCount(steps+10);
    linkerMotor->run(FORWARD);
    rechterMotor->run(FORWARD);
    while(testStep())
    {
        delayMicroseconds(10);
    }
}

void Linksfahren(int steps = 1) {
    setStepCount(steps+10);
    linkerMotor->run(BACKWARD);
    rechterMotor->run(FORWARD);
    while(testStep())
    {
        delayMicroseconds(10);
    }
}

void Rechtsfahren(int steps = 1) {
    setStepCount(steps+10);
    linkerMotor->run(FORWARD);
    rechterMotor->run(BACKWARD);
    while(testStep())
    {
        delayMicroseconds(10);
    }
}

void Zurueckfahren(int steps = 1) {
    setStepCount(steps+10);
    linkerMotor->run(BACKWARD);
    rechterMotor->run(BACKWARD);
    while(testStep())
    {
        delayMicroseconds(10);
    }
}

void MotorStop() {
   linkerMotor->run(RELEASE);
   rechterMotor->run(RELEASE);
}

#endif
