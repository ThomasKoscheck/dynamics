//----- Variablendefinition -----//
#define PI 3.1415926535897932384626433832795
const uint16_t stepspruefung = 10;       //Stepanzahl bei doppelter Pruefung bei ssw und wss
const uint16_t zulangweiss = 4000;  //Wenn zu lange weiss, Suchfunktion (10sec)
volatile bool hindernis = false;
const int Drehung90Grad = 440;
const int ledWeissPin = 14;
const int einKaestchen = 70;

//----- Farbwerte -----//
/* Weiss: true; Schwarz: false */

//-----Sensorwerte der Farbsensoren -----//
extern short farLeftType;
extern short farRightType;
extern bool farLeftBool;
extern bool leftBool;
extern bool midBool;
extern bool rightBool;
extern bool farRightBool;

//----- Libraries -----//
#include "MotorsNXT.h"
#include "Drive.h"
#include "Liniensensor.h"

//----- Initialisierungen -----//
extern void StepperInit();
extern void Vorwaertsfahren(int steps);
extern void Zurueckfahren(int steps);
extern void Linksfahren(int steps);
extern void Rechtsfahren(int steps);
extern void turnleft(void);
extern void turnright(void);
extern void hindernisAusweichen();
extern bool getOldLeft();
extern bool getOldRight();
extern void hardGreenCircle();
extern void Reinigung();
// ----------------------------------------------- Methoden ----------------------------------------------- //

//----- Setup -----//
void timeDiag(void)  {
  static uint32_t time = 0;
  Serial.print("Dauer: ");
  Serial.println(micros()-time);
  time = micros();
}

void sout(String output) {
  Serial.println(output);
}

void soutB(bool output) {
  Serial.print(output);
}

void hindernisInterrupt(void) {    //Tastendruecken
  hindernis = true;
}

void setup() {
  Serial.begin(115200);
  StepperInit();
 // Reinigung();
  Serial.println("Setup");
  pinMode(3, INPUT_PULLUP);
  attachInterrupt(1, hindernisInterrupt, FALLING); //Interrupt-Pin 1 = Arduino-Pin 3
  pinMode(ledWeissPin, OUTPUT);
}

void sensorDiagnose(void) {
    timeDiag();
    //delay(200);
    Serial.println(hindernis);
    Serial.println("Sensordiagnoseoutput");
    Serial.println(farLeftBool);
    Serial.println(leftBool);
    Serial.println(midBool);
    Serial.println(rightBool);
    Serial.println(farRightBool);
    Serial.println(analogRead(2));
    Serial.println(analogRead(3));
    Serial.println(analogRead(8));
    Serial.println(analogRead(9));
    Serial.println(analogRead(10));
    Serial.println("_");
}

bool whitesearch(void)  {
    LineUpdate();
    return leftBool && midBool && rightBool && !hindernis;
}

// ----------------------------------------------- Main loop ----------------------------------------------- //
void loop() {
while(true){ //für break benötigt

    LineUpdate();   //laesst den Sensor "sensor" die Daten in den Variablen Typ (siehe oben) aktualisieren

    //------- weiss-schwarz-weiss -------//
    if(leftBool && !midBool && rightBool && !hindernis)  {
        Serial.println("wsw");
        Vorwaertsfahren(1);
        LineUpdate();
    }

    //------- Hindernis -------//
    else if(hindernis) {
        digitalWrite(ledWeissPin, HIGH);
        Serial.println("Hindernis wurde erkannt, ausweichen eingeleitet");
        hindernisAusweichen();
        digitalWrite(ledWeissPin, LOW);
        hindernis = false;
      }


      //------- weiss-weiss-weiss-weiss-weiss ------//
      else if(farLeftBool && leftBool && midBool && rightBool && farRightBool)    //muss modularisiert werden
      {
        Serial.println("wwwww");
        uint16_t durationTimer = millis();
        while(farLeftBool && leftBool && midBool && rightBool &&farRightBool && millis()-durationTimer < zulangweiss)   {   //bricht ab sobald schwarz //optimierbar
                Vorwaertsfahren(stepspruefung);
                LineUpdate();
        }
        if(millis() - durationTimer < zulangweiss)  {
          Serial.println("Line found by driving forward");
        }
        else    {
                uint16_t stepSearchResolution = 200;  // wird nirgend verwendet oder?
                for(byte i = 3; i!=0 && whitesearch(); i--) {   //Durchläufe der Wischsuche
                    Serial.print("Searching Line, Iteration: ");
                    Serial.println(i);
                    for(uint16_t stepfactor =40; stepfactor != 0 && whitesearch(); stepfactor--)   {//wischt zuerst links
                        Linksfahren();
                    }
                    for(uint32_t stepfactor = 160; stepfactor != 0 && whitesearch(); stepfactor--)    { //für Idioten:  stepfactor definiert die Strecke und die Menge der Suchvorgänge
                        Vorwaertsfahren();
                        stop();
                    }
                    for(uint32_t stepfactor = 160; stepfactor != 0 && whitesearch(); stepfactor--)    {
                        Zurueckfahren();
                        stop();
                    }
                    for(uint16_t stepfactor = 80; stepfactor != 0 && whitesearch();    stepfactor--)    {
                        Rechtsfahren();
                    }
                    for(uint32_t stepfactor = 160; stepfactor != 0 && whitesearch(); stepfactor--)  {
                        Vorwaertsfahren();
                        stop();
                    }
                    for(uint32_t stepfactor = 160; stepfactor != 0 && whitesearch(); stepfactor--)  {
                        Zurueckfahren();
                        stop();
                    }
                    for(uint16_t stepfactor = 40; stepfactor != 0 && whitesearch(); stepfactor--)   {
                        Linksfahren();
                        stop();
                    }
                    for(uint16_t stepfactor = 580; stepfactor != 0 && whitesearch(); stepfactor--)  {
                        Vorwaertsfahren();
                    }
                }
        }
    }


    //------- weiss-weiss-schwarz -------//
    else if(leftBool && midBool && !rightBool) {
        Serial.println("wws");
        turnright();       //dreht solang nach rechts bis wieder auf schwarz
      	LineUpdate();
    }

    //------- schwarz-weiss-weiss -------//
    else if(!leftBool && midBool && rightBool) {
        Serial.println("sww");
        turnleft();     //dreht solang nach links bis wieder auf schwarz
      	LineUpdate();
    }

    //------- schwarz-schwarz-schwarz-weiss-weiss -------//
    else if (!farLeftBool && !leftBool && !midBool && rightBool && farRightBool) {
      Serial.println("sssww");
      Vorwaertsfahren(einKaestchen);
      LineUpdate();
      if (midBool){
        turnleft();
      }
    }

    //------- weiss-weiss-schwarz-schwarz-schwarz -------//
    else if (farLeftBool && leftBool && !midBool && !rightBool && !farRightBool) {
      Serial.println("wwsss");
      Vorwaertsfahren(einKaestchen);
      LineUpdate();
      if (midBool){
        turnright();
      }
    }

    //------- schwarz-schwarz-weiss -------//
    else if(!leftBool && !midBool && rightBool) {
        Serial.println("ssw");
        Vorwaertsfahren(stepspruefung);
        LineUpdate();
        if(!(!leftBool && !midBool && rightBool)){
          Serial.println("break");
          break;
        }
        Vorwaertsfahren(einKaestchen-stepspruefung);
        LineUpdate();

        //------- weiss-weiss-weiss -------//
        if(leftBool && midBool && rightBool) //90 Gradkurve oder normale (runde) Linkskurve
        {
          Serial.println("  www");
          turnleft();       //dreht solang nach links bis wieder auf schwarz
          LineUpdate();
        }

        //------- weiss-schwarz-weiss -------//
        else if(leftBool && !midBool && rightBool) //Kreuzung, muss gerade weiter fahren
        {
          Serial.println("  wsw");
          Vorwaertsfahren(einKaestchen);
          LineUpdate();
        }

        //------- schwarz-schwarz-schwarz -------//
        else if(!farLeftBool && !leftBool && !midBool)  //gruener Punkt links
        {
          Serial.println("  gruener Punkt4");
          Linksfahren(Drehung90Grad);
          Vorwaertsfahren(einKaestchen);
        }

        //------- schwarz-schwarz-weiss -------//
        else if(!farLeftBool && !leftBool && midBool) //gruner Punkt links
        {
          Serial.println("  gruener Punkt3");
          Linksfahren(Drehung90Grad);
          Vorwaertsfahren(einKaestchen);
        }



    //------- weiss-schwarz-schwarz -------//
    else if(leftBool && !midBool && !rightBool) {
        Serial.println("wss");
        Vorwaertsfahren(stepspruefung);
        LineUpdate();
        if(!(leftBool && !midBool && !rightBool)){
          Serial.println("break");
          break;
        }
        Vorwaertsfahren(einKaestchen-stepspruefung);
        LineUpdate();

        //------- weiss-weiss-weiss -------//
        if(leftBool && midBool && rightBool) //90 Gradkurve oder normale (runde) Rechtskurve
        {
          Serial.println("  www");
          turnright();       //dreht solang nach links bis wieder auf schwarz
          LineUpdate();
        }

        //------- weiss-schwarz-weiss -------//
        else if(leftBool && !midBool && rightBool) //Kreuzung, muss gerade weiter fahren
        {
          Serial.println("  wsw");
          Vorwaertsfahren(einKaestchen);
          LineUpdate();
        }

        //------- schwarz-schwarz-schwarz -------//
        else if(!farLeftBool && !leftBool && !midBool)  //gruener Punkt rechts
        {
          Serial.println("  gruener Punkt1");
          Rechtsfahren(Drehung90Grad);
          Vorwaertsfahren(einKaestchen);

        }


        //------- weiss-schwarz-schwarz -------//
        else if(farLeftBool && !leftBool && !midBool) //gruner Punkt rechts
        {
          Serial.println("  gruener Punkt2");
          Rechtsfahren(Drehung90Grad);
          Vorwaertsfahren(einKaestchen);
    }


    //------- schwarz-weiss-weiss-weiss-weiss -------//
    else if(!farLeftBool && leftBool && midBool && rightBool && farRightBool) {
        Serial.println("swwww");
        Rechtsfahren(80);   //dreht sonst auf Linie um
        Vorwaertsfahren(100);
        turnleft();  //dreht solang nach links bis wieder auf schwarz
        LineUpdate();
    }

    //------- weiss-weiss-weiss-weiss-schwarz -------//
    else if(farLeftBool && leftBool && midBool && rightBool && !farRightBool) {
        Serial.println("wwwws");
        Linksfahren(80);   //dreht sonst auf Linie um
        Vorwaertsfahren(100);
        turnright();  //dreht solang nach rechts bis wieder auf schwarz
        LineUpdate();
    }


    //------- schwarz-schwarz-schwarz-schwarz-schwarz -------//
    else if((!farLeftBool && !leftBool && !midBool && !rightBool && !farRightBool) || (!leftBool && !midBool && !rightBool)) {
        Serial.println("sssss");
        Vorwaertsfahren(4*einKaestchen);
        LineUpdate();

        //Alternative Methode
        /*if (!(!leftBool || !midBool || !rightBool)){  //wenn keine schwarze Linie da ist -> nicht möglich, deswegen zurückfahren
           Zurueckfahren(3*einKaestchen);
        }
        LineUpdate();*/
    }

    //------- nicht dokumentierter Fall -------//
    else {
       Serial.println("----");
       Vorwaertsfahren(stepspruefung);
       LineUpdate();
    }

}
}
}
}
