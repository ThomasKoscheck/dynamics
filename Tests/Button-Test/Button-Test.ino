const int PIN_OUT = 2;
bool hit = false;
//PIN_OUT wird mit rotem Kabel verbunden
//GND wird mit grauen Kabel direkt neben dem roten verbunden
//Kabelbelegung: https://www.wayneandlayne.com/bricktronics/design-and-theory/

#include "Wire.h"
#include "Adafruit_MotorShield.h" //Motorshield

Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x60);
Adafruit_StepperMotor *liMot = AFMS.getStepper(200,1);
Adafruit_StepperMotor *reMot = AFMS.getStepper(200,2);

void stoppen() {
  detachInterrupt(0);
  Serial.println("Treffer");
  attachInterrupt(0, stoppen, FALLING);
  hit = true;
}

void setup() {
  Serial.begin(115200);
  pinMode(PIN_OUT, INPUT_PULLUP);  //Wichtig: Pullup!!
  AFMS.begin(1600);
  liMot->setSpeed(250);
  reMot->setSpeed(250);
  attachInterrupt(0, stoppen, FALLING);
}

void loop() {
  if(!hit)  {
    liMot->step(2, FORWARD, DOUBLE);
    reMot->step(2, FORWARD, DOUBLE);
  }
  else  {
    liMot->step(50, BACKWARD, DOUBLE);
    reMot->step(50, BACKWARD, DOUBLE);
  }
  Serial.println(hit);
}
