const int LedRot = 21;
const int LedGruen = 20;
const int LedWeiss = 19; //Weiße LED ist sehr hell und einet sich gut für Tests

void setup() {
 pinMode(LedRot, OUTPUT);
 pinMode(LedGruen, OUTPUT);
 pinMode(LedWeiss, OUTPUT); 
}

void loop() {
  digitalWrite(LedRot, HIGH);
  digitalWrite(LedGruen, HIGH);  
  digitalWrite(LedWeiss, HIGH);
  delay(500);
  digitalWrite(LedRot, LOW);
  digitalWrite(LedGruen, LOW);  
  digitalWrite(LedWeiss, LOW);
  delay(500);
}
