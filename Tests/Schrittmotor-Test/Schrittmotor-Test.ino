#include "Wire.h"
#include "Adafruit_MotorShield.h"

Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x60);     //erstellt Motorshield-Objekt mit I2C Adresse 0x60
Adafruit_StepperMotor *linkerMotor = AFMS.getStepper(200, 1);   //1 = M1 & M2
Adafruit_StepperMotor *rechterMotor = AFMS.getStepper(200, 2);  //2 = M3 & M4


void setup() {
  Serial.begin(115200);
  AFMS.begin(1600);              // Motortreiber-PWM-Frequenz wird auf Standartwert von 1600Hz gesetzt
  linkerMotor->setSpeed(300);   // 1000 rpm  
  rechterMotor->setSpeed(300);
  Serial.println("Setup finished");
}


void loop() {
  
 linkerMotor->step(1, BACKWARD, SINGLE);   //als zweites Attribut kann SINGLE, DOUBLE, MICROSTEPS, INTERLEAVE verwendet werden
 rechterMotor->step(1, FORWARD, SINGLE);   //360° = 200 Steps  -->  1 Step = 1.8°
}
