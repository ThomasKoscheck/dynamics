short farLeftPin=3;  //analog Pin
short leftPin=9;      //digital Pin
short midPin=2;       //analog Pin
short rightPin=8;     //digital Pin
short farRightPin=1;  //analog Pin

short s2Pin=6;        //digital Pin
short s3Pin=7;        //digital Pin

short farLeftValue = 0;
short leftValue = 0;
short midValue = 0;
short rightValue = 0;
short farRightValue = 0;

bool farLeftBool;
bool leftBool;
bool midBool;
bool rightBool;
bool farRightBool;

void LineUpdate();
short leftRightBorderIR = 100;
short midBorderIR = 150;
short borderFarb = 9;

void setup() {
 Serial.begin(115200);
 digitalWrite(s2Pin, HIGH); //S2=HIGH und S3=HIGH  --> gruen-Wert wird aufgenommen
 digitalWrite(s3Pin, HIGH);
}

void loop() {
  farLeftValue = analogRead(farLeftPin);
  midValue = analogRead(midPin);
  farRightValue = analogRead(farRightPin);
  
  farLeftBool = analogRead(farLeftPin) >= leftRightBorderIR ? true : false;
  midBool = analogRead(midPin) >= midBorderIR ? true : false;
  farRightBool = analogRead(farRightPin) >= leftRightBorderIR ? true : false;
  
  leftValue = pulseIn(leftPin, digitalRead(leftPin) == HIGH ? LOW : HIGH);
  leftBool =  leftValue <= borderFarb ? true : false;
  
  rightValue = pulseIn(rightPin, digitalRead(rightPin) == HIGH ? LOW : HIGH);
  rightBool =  rightValue <= borderFarb ? true : false;

  Serial.println(farLeftValue);
  Serial.println(leftValue);  
  Serial.println(midValue);
  Serial.println(rightValue);
  Serial.println(farRightValue);
  Serial.println();
  
  Serial.println(farLeftBool);
  Serial.println(leftBool);  
  Serial.println(midBool);
  Serial.println(rightBool);
  Serial.println(farRightBool);
  Serial.println("-----");
  delay(2000);
}
