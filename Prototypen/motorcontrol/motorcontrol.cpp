#include "motorcontrol.h"
#include "Arduino.h"

Adafruit_MotorShield Shield = Adafruit_MotorShield(0x60);

motorcontrol::void motorcontrol(byte leftStepper, byte rightStepper)
 {
      this->type = true;
      static Adafruit_StepperMotor _leftMotor = Shield.getStepper(200,leftStepper);
      static Adafruit_StepperMotor _rightMotor = Shield.getStepper(200, rightStepper);
      leftMotor = &_leftMotor;
      rightMotor = &_rightMotor;
}

motorcontrol::void motorcontrol(byte leftEncoder, byte rightEncoder, byte leftMotor, byte rightMotor)   {
    this->type = false;
    static nxtDC _leftMotor = nxtDC(&Shield, byte leftEncoder, byte leftMotor);
    static nxtDC _rightMotor = nxtDC(&Shield, byte rightEncoder, byte rightMotor);
    leftMotor = &_leftMotor;
    rightMotor = &_rightMotor;
}

motorcontrol::void Init(void)    {
  Shield.begin();

  if(type)
  {

  }
  else
  {
      vor = &nxtVor;
      zurueck = &nxtZurueck;
      links = &nxtLinks;
      rechts = &nxtRechts;
  }
}

motorcontrol::void Vorwaertsfahren(int steps = 50)    {
    (*vor_)(steps);
}

motorcontrol::void Zurueckfahren(int steps = 50)  {
    (*zurueck_) (steps);
}

motorcontrol::void Linksfahren(int steps=50)   {
    (*links_) (steps);
}

motorcontrol::void Rechtsfahren(int steps=50)   {
    (*rechts_)(steps);
}

nxtDC::nxtDC(Adafruit_MotorShield *Shield, byte encoder, byte connector)    {
    Adafruit_DCMotor motor = *Shield.getMotor(connector);
    attachInterrupt(encoder, encode, FALLING);

}

motorcontrol::void nxtVor(int steps)
{
    leftMotor->vor(steps);
    rightMotor->vor(steps);
    wait();
}

motorcontrol::void nxtLinks(int steps)
{
    leftMotor->zurueck(steps);
    rightMotor->vor(steps);
    wait();
}

motorcontrol::void nxtRechts(int steps)  {
    leftMotor->vor(steps);
    rightMotor->zurueck(steps);
    wait();
}

motorcontrol::void nxtZurueck(int steps)
{
    leftMotor->zurueck(steps);
    rightMotor->zurueck(steps);
    wait();
}

motorcontrol::void wait()
{
    while(*leftMotor.testStep() && *rightMotor.testStep)  {
        delayMicroseconds(10);
    }
    if(*leftMotor.testStep())  {
        leftMotor->stop();
    }
    if(*rightMotor.testStep())  {
        rightMotor->stop();
    }
    delay(1);
    if(*rightMotor.testStep||*leftMotor.testStep())
    {
        wait();
    }
}


nxtDC:steps = 0;        //n�tig?

nxtDC::void encode(void) {
    steps--;
}

nxtDC::setSteps(uint32_t stepcount)   {
    this->steps =stepcount;
}

nxtDC::bool testStep(void)  {
    return steps>10;
}


nxtDC::void _vor(void)   {
    motor->run(FORWARD);
}

nxtDC::void _zurueck(void)  {
    motor->run(BACKWARD);
}

nxt void zurueck(uint32_t steps = 50){
    setSteps(steps);
    _zurueck();
}

nxtDC:void vor(uint32_t steps = 50) {
    setSteps(steps);
    _vor();
}

nxtDC::void stop(void)  {
    motor->run(RELEASE);
}
