#ifndef motorcontrol_h
#define motorcontrol_h
#include "Arduino.h"
#include "Adafruit_MotorShield.h"

class motorcontrol  {
public:
    motorcontrol(byte leftEncoder, byte rightEncoder, byte leftMotor, byte rightMotor);
    motorcontrol(byte leftStepper, byte rightStepper);
    void vor(int steps);
    void zurueck(int steps);
    void links(int steps);
    void rechts(int steps);
    void Init(void);

private:
    Adafruit_MotorShield Shield;
    void *leftMotor;
    void *rightMotor;
    bool type;
    void (*vor_) (int);
    void (*zurueck_) (int);
    void (*links_) (int);
    void (*rechts_) (int);
    void nxtVor(int);
    void nxtZurueck(int);
    void nxtLinks(int);
    void nxtRechts(int);
    void wait(void);


};


class nxtDC{
public:
    nxtDC(Adafruit_MotorShield *Shield, byte encoder, byte connector);
    bool testStep(void);
    void setSteps(uint32_t stepcount);
    void stop(uint32_t steps);
    void vor(uint32_t steps);
    void zurueck(void);

private:
    volatile uint_fast32_t steps;
    void encode(void);
    void _vor(void);
    void _zurueck(void);

};

#endif
