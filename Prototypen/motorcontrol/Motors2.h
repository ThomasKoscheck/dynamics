#ifndef motor2_h
#define mtor2_h
#include "Adafruit_MotorShield.h"
volatile uint_fast32_t steps = 0;

Adafruit_MotorShield Shield = Adafruit_MotorShield(0x60);
Adafruit_DCMotor *linkerMotor = Shield.getMotor(1);
Adafruit_DCMotor *rechterMotor= Shield.getMotor(3);

void encode(void)   {
    steps--;
}

bool testStep(void) {
    return steps > 10;
}

void setStepCount(uint_fast32_t stepCount)  {
    steps = stepCount;
}

void StepperInit()  {
    Shield.begin();
    linkerMotor->setSpeed(250);
    rechterMotor->setSpeed(250);
    attachInterrupt(1, encode, FALLING);
}

void Vorwaertsfahren(int steps = 50) {
    setStepCount(steps+10);
    linkerMotor->run(FORWARD);
    rechterMotor->run(FORWARD);
    while(testStep)
    {
        delayMicroseconds(10);
    }
}

void Linksfahren(int steps = 50) {
    setStepCount(steps+10);
    linkerMotor->run(BACKWARD);
    rechterMotor->run(FORWARD);
    while(testStep)
    {
        delayMicroseconds(10);
    }
}

void Rechtsfahren(int steps = 50) {
    setStepCount(steps+10);
    linkerMotor->run(BACKWARD);
    rechterMotor->run(FORWARD);
    while(testStep)
    {
        delayMicroseconds(10);
    }
}

void Zurueckfahren(int steps = 50) {
    setStepCount(steps+10);
    linkerMotor->run(BACKWARD);
    rechterMotor->run(BACKWARD);
    while(testStep)
    {
        delayMicroseconds(10);
    }
}
#endif // motor2_h
