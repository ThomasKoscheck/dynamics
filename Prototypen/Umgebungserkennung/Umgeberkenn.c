//----- Datentypdefinition -----
struct messwert{
  byte abstand;
  byte winkel;
  int x;    //Speicheroptimierung --> nimmt berechneten Wert x100
  int y;    //siehe x
  int xvektor;
  int yvektor;
};



//----- Variablendefinition -----
#define PI 3.1415926535897932384626433832795
#define USP 0 //Ultraschallsensorpin
const int stepspruefung = 10;       //Stepanzahl bei doppelter Prüfung bei ssw und wss
const byte messbereich = 180; //90grad Messbereich - Speichersparen
const byte messabstand = 1; //Auflösung des Umgebungsscan in °
const byte messwertmenge = 180; //Messbereich/Messabstand

messwert eingang[messwertmenge];

//----- Libraries & Initialisierungen -----
#include "Wire.h" //I²C
#include "Servo.h"
Servo servo;



// ----------------------------------------------- Methoden ----------------------------------------------- //

//----- SETUP -----
void setup() {
  servo.attach(9);
  Serial.begin(115200);      
  Serial.println("SETUP");    
  Serial.println(millis());
}


//----- Objekterkennung -----
void messwerteingabe() {
  for(byte i=0;i<messwertmenge;i++)  {       //nicht optimierter Code !! - Muss mit Pointern optimiert werden!
    eingang[i*messabstand].winkel = messabstand*i;
    eingang[i].abstand = messung(eingang[i].winkel);    // 2 Methodenmessung    
    eingang[i].x = xumrechnung(eingang[i].winkel, eingang[i].abstand);
    eingang[i].y = yumrechnung(eingang[i].winkel, eingang[i].abstand);
    
    if(i!=0)  {
      vektorisierung(i);
    }
    delay(1); //Delay für Watchdogtimer
  }  
}

int messung(byte winkel)  {
  winkel<=180 ? servomove(winkel):servomove(90);
  delay(100);
  Serial.println(millis());
  int yabstand = 40;
    winkel -= messbereich/2;
    winkel = (byte)sqrt(winkel*winkel);
    Serial.print("Winkel: ");
    Serial.println(winkel);
    //float abstand = distmessung(winkel);
    float abstand = sqrt((yabstand*yabstand)+(tan(winkel)*yabstand)*(tan(winkel)*yabstand)); //MUSS NOCH GEMACHT WERDEN
    return (int)abstand;
}

int xumrechnung(int winkel, int abstand){     
  return (int)((sin(((((messbereich/2)+winkel)/180.0)*PI))*abstand*100));
}

int yumrechnung(int winkel, int abstand){     
  return (int)((cos(((((messbereich/2)+winkel)/180.0)*PI))*abstand*100));
}

void vektorisierung(byte i){
  byte next = (i-1);
  eingang[i].xvektor = eingang[next].x - eingang[i].x;
  eingang[i].yvektor = eingang[next].y - eingang[i].y;
}

void objektscan() {
  messwerteingabe();            // Erklärung der seriellen Ausgabe
  for(int i=0;i<messbereich;i++)  {
    Serial.print("aktuelle Messung: ");
    Serial.print("Winkel: ");
    Serial.println(i);
    Serial.println(eingang[i].winkel);
    Serial.print("Abstand Total/x/y");
    Serial.println(eingang[i].abstand);
    Serial.println(eingang[i].x);
    Serial.println(eingang[i].y);
    Serial.print("Vektoren x / y / Quotient");
    Serial.println(eingang[i].xvektor);
    Serial.println(eingang[i].yvektor);
    Serial.println((float)eingang[i].xvektor/eingang[i].yvektor);
    Serial.println("----");
    delayMicroseconds(50);
  }
  delay(1);
}


void servomove(byte winkel)  {
  winkel += (180-messbereich)/2;
  Serial.print("Servo moving to position: ");
  Serial.println(winkel);
  servo.write(winkel);
}

void loop() {
  objektscan();
  delay(10000);
}






